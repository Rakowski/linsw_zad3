import os
import tornado.ioloop
import tornado.web
import base64

FILES="/mnt/files/"

class Index(tornado.web.RequestHandler):
    def get(self):
        print "List files request"

	files = []
	for filename in os.listdir(FILES):
		files.append(filename)
	self.render('index.html', files=files)

    def post(self):
        auth_hdr = self.request.headers.get('Authorization')
        print auth_hdr
        if auth_hdr == None or not auth_hdr.startswith('Basic '):
            return self.auth()

        auth_decoded = base64.decodestring(auth_hdr[6:])
        username, password = auth_decoded.split(':', 2)

        if username != "ja" or password != "ja":
            return self.auth()

        f = self.request.files['datafile'][0]
        filename = f['filename']
    	
        print "Upload request with file: " + filename
        x = open(FILES + filename, 'w')
    	x.write(f['body'])
    	x.close()

    	self.redirect("/")

    def auth(self):
        self.set_status(401)
        self.set_header('WWW-Authenticate', 'Basic realm=Fileserver')
        self.finish()
        return False

class Download(tornado.web.RequestHandler):
	def get(self, filename):
            print "Download request for file: " + filename
            x = open(FILES + filename)
            self.set_header('Content-Type', 'text/csv')
	    self.set_header('Content-Disposition', 'attachment; filename="' + filename + '"')
	    self.finish(x.read())

if __name__ == "__main__":
    webserv = tornado.web.Application(
            [   (r"/", Index),
                (r"/download/(.+)", Download)])

    webserv.listen(80)
    tornado.ioloop.IOLoop.instance().start()
