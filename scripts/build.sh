#!/bin/bash
admin=${BR}-admin
user=${BR}-user

# get buildroot
wget https://buildroot.org/downloads/${BR}.tar.bz2

# unpack and clone buildroot for -admin and -user versions
tar -xjf ${BR}.tar.bz2
cp -r ${BR} ${admin}
mv ${BR} ${user}

# unpack custom buildroot configurations
tar -jf rakowskip_zad3.tar.bz2 -x ${admin} ${user} lock_src

# build
cd ${admin}
make
cd ../${user}
make
