#!/bin/bash

echo "Preparing ${SD} device"

# partition sizes
mb_to_sectors=2048
p1_size_mb=12
p2_size_mb=20
let "p1_size_sec = ${p1_size_mb} * ${mb_to_sectors}"
let "p2_size_sec = ${p2_size_mb} * ${mb_to_sectors}"

# partition types
p_fat16=e
p_linux=83

# partition
sfdisk -q ${SD} << EOF
,${p1_size_sec},${p_fat16}
,${p2_size_sec},${p_linux}
,,${p_linux}
EOF

# build filesystems
mkfs.vfat -F16 ${SD}1
mkfs.ext4 ${SD}2
mkfs.ext4 ${SD}3

# copy data onto SD card
imgs=output/images
zImage=${imgs}/zImage
admin=${BR}-admin
user=${BR}-user

# first partition: zImages, DTB, boot
p1=${MP}/p1
mkdir -p ${p1}
mount ${SD}1 ${p1}
cp ../${admin}/${zImage} ${p1}/zImage.admin
cp ../${user}/${zImage} ${p1}/zImage.user
cp ../${imgs}/rpi-firmware/* ${p1}
cp ../${imgs}/bcm2708-rpi-b.dtb ${p1}
umount ${p1}

p2=${MP}/p2
mkdir -p ${p2}
mount ${SD}2 ${p2}
# rootfs
#tar -xjf ../${BR}-user/${imgs}/rootfs.tar.bz2 -C ${p2}
umount ${p2}
